/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqXpressTests;

import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import paqxpress.core.Cliente;
import paqxpress.core.Paquete;
import paqxpress.core.PqManager;

/**
 *
 * @author cisco
 */
public class pqManagerTest {
    
    public pqManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    
    @Test
    public void altaClienteTest() {
        System.out.println("1-> dar de alta un cliente");
        PqManager instance= new PqManager();
        Cliente cli = new Cliente("1","Fran");
        instance.altaCliente("1", "Fran");
        assertFalse(instance.getClientes().isEmpty());
        assertEquals("["+cli+ "]",instance.getClientes().toString());
        assertEquals(1, instance.getClientes().size());
    }
    //devolverá un ​ array ​ con los clientes
    @Test
    public void listaClientesTest() {
        System.out.println("2-> mostrar lista de clientes");
        PqManager instance= new PqManager();
    }
    @Test
    public void altaEnvioTest() {
        System.out.println("3-> dar de alta un envio");
        PqManager instance= new PqManager();
        Cliente cli = new Cliente("1","Fran");
        instance.altaEnvio(cli, "Ruben", "San Clemente");
        assertFalse(instance.getPaquetes().isEmpty());
        assertEquals(1,instance.getPaquetes().size());        
    }
    //devolverá una cadena con la descripción del estado actual
    @Test
    public void estadoPedidoTest() {
        System.out.println("4-> ver el estado de un pedio");
        PqManager instance= new PqManager();
        Cliente cli = new Cliente("1","Fran");
        instance.altaEnvio(cli, "Ruben", "San Clemente");
        int id= 1004;
        assertEquals("El paquete "+instance.getPaquetes().get(id).getId()+" "+"Se ha realizado el pedido con exito", instance.estadoPedido(id));
        instance.getPaquetes().get(id).cambiaEstado();
        assertEquals("El paquete "+instance.getPaquetes().get(id).getId()+" "+"el pedido se esta procesando en el almacen", instance.estadoPedido(id));
        instance.getPaquetes().get(id).cambiaEstado();
        assertEquals( "El paquete "+instance.getPaquetes().get(id).getId()+" "+"el pedido acaba de salir del almacen", instance.estadoPedido(id));
        instance.getPaquetes().get(id).cambiaEstado();
        assertEquals("El paquete "+instance.getPaquetes().get(id).getId()+" "+"El pedido se encuentra de camino al destino", instance.estadoPedido(id));        
    }
    //devolverá un ​ array​ con los paquetes en reparto
    @Test
    public void  listaRepartoTest() {
        System.out.println("5-> mostrar lista de reparto");
        PqManager instance= new PqManager();
        Cliente cli = new Cliente("1","Fran");
        int id= 1002;
        instance.altaEnvio(cli,"Pedro","madrid");
        System.out.println(instance.getPaquetes());
        instance.getPaquetes().get(id).cambiaEstado();
        instance.getPaquetes().get(id).cambiaEstado();
        instance.getPaquetes().get(id).cambiaEstado();
        assertFalse(instance.listaReparto().length==0);
        assertEquals( "El paquete "+instance.getPaquetes().get(id).getId()+" "+"El pedido se encuentra de camino al destino", instance.estadoPedido(id));
    }
    //devolverá un ​ array​ de los paquetes enviados por el cliente indicado
    @Test
    public void listaEnviosClienteTest() {
        System.out.println("6-> mostrar lista de envios a clientes");
        PqManager instance= new PqManager();
        Cliente cli = new Cliente("1","Fran");
        instance.altaEnvio(cli,"Pedro","madrid");
        assertFalse(instance.listaEnviosCliente("1").length==0);
        
    }
}
