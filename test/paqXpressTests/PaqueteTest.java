/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqXpressTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import paqxpress.core.Cliente;
import paqxpress.core.Paquete;
import paqxpress.core.estados.Enviado;

/**
 *
 * @author cisco
 */
public class PaqueteTest {
    
    public PaqueteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void testCrearPaquete(){
        System.out.println("1->  crea paquete");
        Paquete p= new Paquete(new Cliente("90309","Fran"),"fulano","rua da praia");
        Paquete p1= new Paquete(new Cliente("9030239","Carlos"),"Mengano","rua da praia");
        Paquete p2= new Paquete(new Cliente("90409","Pepe"),"yonoi","rua da praia");
        Paquete p3= new Paquete(new Cliente("4484909","Pablo"),"portos","rua da praia");
        assertEquals(1001,p.getId());
        assertEquals(1002,p1.getId());
        assertEquals(1003,p2.getId());
        assertEquals(1004,p3.getId());
    }
    @Test
    public void testCambiaEstado(){
        System.out.println("2-> cambia estado");
        Paquete instance= new Paquete(new Cliente("90309","Fran"),"fulano","rua da praia");
        assertEquals("Se ha realizado el pedido con exito", instance.informaEstado());
        instance.cambiaEstado();
        assertEquals("el pedido se esta procesando en el almacen", instance.informaEstado());
        instance.cambiaEstado();
        assertEquals( "el pedido acaba de salir del almacen", instance.informaEstado());
        instance.cambiaEstado();
        assertEquals("El pedido se encuentra de camino al destino", instance.informaEstado());
    }
    @Test
    public void testSetEstado(){
        System.out.println("3-> setea estado");
        Paquete instance= new Paquete(new Cliente("90309","Fran"),"fulano","rua da praia");
        assertEquals("Se ha realizado el pedido con exito", instance.informaEstado());
        instance.setEstado(new Enviado());
        assertEquals( "el pedido acaba de salir del almacen", instance.informaEstado());
    }
    @Test
    public void  testInformaEstado(){
        System.out.println("4-> informa de estado");
        Paquete instance= new Paquete(new Cliente("90309","Fran"),"fulano","rua da praia");
        assertEquals("Se ha realizado el pedido con exito", instance.informaEstado());
    }
}
