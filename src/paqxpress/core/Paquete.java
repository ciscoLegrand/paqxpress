package paqxpress.core;

import paqxpress.core.estados.Ordenado;
import paqxpress.core.estados.EstadoPq;
import java.time.*;
import java.time.format.DateTimeFormatter;
//import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author cisco
 */
public class Paquete {
    // private static final AtomicInteger count = new AtomicInteger(1000);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static int AUTOINCREMENTAL = 1000;
    private int id = 0;
    private LocalDateTime fAlta;
    private Cliente cliente;
    private String destinatario;
    private String destino;
    private EstadoPq estado;

    public Paquete(Cliente pCli, String pDestinatario, String pDestino) {
        // String sId = String.format(""+ count.incrementAndGet());
        // this.id=Integer.parseInt(sId);
        this.id = ++AUTOINCREMENTAL;
        this.cliente = pCli;
        this.destinatario = pDestinatario;
        this.destino = pDestino;
        this.fAlta = LocalDateTime.now();
        this.estado = new Ordenado();
    }

    public int getId() {
        return this.id;
    }

    public String getFAlta() {
        String fecha = fAlta.format(formatter);
        return fecha;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public String getDestinario() {
        return this.destinatario;
    }

    public void setDestinatario(String pDestinatario) {
        this.destinatario = pDestinatario;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String pDestino) {
        this.destino = pDestino;
    }

    public EstadoPq getEstado() {
        return this.estado;
    }

    public void cambiaEstado() {
        getEstado().avanza(this);
    }

    public void setEstado(EstadoPq pEstado) {
        this.estado = pEstado;
    }

    public String informaEstado() {
        return "" + getEstado().informaEstado();
    }

    @Override
    public String toString() {
        String fecha = fAlta.format(formatter);
        return "Paquete " + id + ", fecha de Alta: " + fecha + ", cliente: " + cliente.getName() + ", destinatario: "
                + destinatario + ", destino: " + destino + ", estado: " + informaEstado();
    }

}
