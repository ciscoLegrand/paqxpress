/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqxpress.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author cisco
 */
public class PqManager {
    private List<Cliente> clientes = new ArrayList<>();
    private Map<Integer, Paquete> paquetes = new HashMap<>();

    public List<Cliente> getClientes() {
        return clientes;
    }

    public Map<Integer, Paquete> getPaquetes() {
        return paquetes;
    }

    public Paquete getPaquete(int id) {
        return paquetes.get(id);
    }

    public void altaCliente(String pDni, String pName) {
        clientes.add(new Cliente(pDni, pName));
    }

    public Cliente[] listaClientes() {
        int len = getClientes().size(), cont = 0;
        Cliente[] arrClientes = new Cliente[len];
        for (Cliente cli : clientes)
            arrClientes[cont++] = cli;
        return arrClientes;
    }

    public void altaEnvio(Cliente pCli, String pFrom, String pTo) {
        Paquete paq = new Paquete(pCli, pFrom, pTo);
        paquetes.put((paq.getId()), paq);
    }

    public String estadoPedido(int pIdPaq) {
        return "El paquete " + paquetes.get(pIdPaq).getId() + " " + paquetes.get(pIdPaq).informaEstado();
    }

    private Paquete[] listaDePaquetes() {
        int len = paquetes.size(), cont = 0;
        Paquete[] lp = new Paquete[len];
        Set<Integer> clave = paquetes.keySet();
        for (Integer paq : clave)
            if (paq.equals(paquetes.get(paq).getId()))
                lp[cont++] = paquetes.get(paq);
        return lp;
    }

    public Paquete[] listaReparto() {
        int cont = 0, len = listaDePaquetes().length;
        Paquete[] lp = new Paquete[len];
        for (Paquete paq : listaDePaquetes())
            if (paq.informaEstado().equals("El pedido se encuentra de camino al destino"))
                lp[cont++] = paq;
        return lp;
    }

    public Paquete[] listaEnviosCliente(String pDni) {
        int cont = 0, len = listaDePaquetes().length;
        Paquete[] lp = new Paquete[len];
        for (Paquete paq : listaDePaquetes())
            if (paq.getCliente().getId().equals(pDni))
                lp[cont++] = paq;
        return lp;
    }

}
