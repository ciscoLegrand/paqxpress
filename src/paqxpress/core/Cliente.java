package paqxpress.core;

/**
 *
 * @author cisco
 */
public class Cliente {

    private String id;
    private String name;

    public Cliente(String pId, String pName) {
        this.id = pId;
        this.name = pName;
    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "DNI/NIF: " + id + " Nombre: " + name.toUpperCase();
    }
}
