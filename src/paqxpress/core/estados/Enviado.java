/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqxpress.core.estados;

import paqxpress.core.Paquete;

/**
 *
 * @author cisco
 */
public class Enviado implements EstadoPq{

    @Override
    public void avanza(Paquete pPaquete) {
       pPaquete.setEstado(new EnReparto());
    }

    @Override
    public String informaEstado() {
        return "el pedido acaba de salir del almacen";
    }
    
}
