/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqxpress.core.estados;

import paqxpress.core.Paquete;

/**
 *
 * @author cisco
 */
public class EnProceso implements EstadoPq{

    @Override
    public void avanza(Paquete pPaquete) {
        pPaquete.setEstado(new Enviado());
    }

    @Override
    public String informaEstado() {
        return "el pedido se esta procesando en el almacen";
    }
    
}
