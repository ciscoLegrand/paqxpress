/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqxpress.core.estados;

import paqxpress.core.Paquete;

/**
 *
 * @author cisco
 */
public interface EstadoPq {
//    public abstract Paquete getPaquete(int id);
    public abstract void avanza(Paquete pPaqute);
    public abstract String informaEstado();
}
