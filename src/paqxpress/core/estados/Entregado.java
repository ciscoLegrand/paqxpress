/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqxpress.core.estados;

import paqxpress.core.Paquete;

/**
 *
 * @author cisco
 */
public class Entregado implements EstadoPq{

    @Override
    public void avanza(Paquete pPaquete) {
        pPaquete.setEstado(this);
    }

    @Override
    public String informaEstado() {
        return " el pedido ya se ha entregado en destino";
    }
    
}
