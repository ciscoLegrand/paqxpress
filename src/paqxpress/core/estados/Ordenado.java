/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqxpress.core.estados;

import paqxpress.core.Paquete;
import paqxpress.core.estados.EstadoPq;

/**
 *
 * @author cisco
 */
public class Ordenado implements EstadoPq{

    @Override
    public void avanza(Paquete pPaquete) { 
        pPaquete.setEstado(new EnProceso());
    }

    @Override
    public String informaEstado() {
        return "Se ha realizado el pedido con exito";
    }
    
}
