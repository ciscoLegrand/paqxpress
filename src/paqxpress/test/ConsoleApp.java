package paqxpress.test;

//import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import paqxpress.core.Cliente;
import paqxpress.core.Paquete;
import paqxpress.core.PqManager;
import paqxpress.core.estados.*;

/**
 *
 * @author cisco
 * @version 1.5
 */
public class ConsoleApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConsoleApp app = new ConsoleApp();
        app.mainMenu();

    }

    Scanner s = new Scanner(System.in);
    PqManager manager = new PqManager();
    Paquete paquete;

    public void datosDefault() {
        manager.altaCliente("14990502p", "Fran");
        manager.altaCliente("24920647m", "Alberto");
        manager.altaCliente("44910282w", "Cata");
        manager.altaCliente("64950251v", "Carlos");
        manager.altaCliente("x9102938", "Andres");
        manager.altaEnvio(manager.getClientes().get(0), "Ruben", "rua blanco amor n4");
        manager.altaEnvio(manager.getClientes().get(0), "paula", "avenida vilagarcia n16");
        manager.altaEnvio(manager.getClientes().get(1), "pablo", "lugar do pardiño n2");
        manager.altaEnvio(manager.getClientes().get(0), "jorge", "rua santa clara n9");
        manager.getPaquetes().get(1001).setEstado(new EnReparto());
        manager.getPaquetes().get(1002).setEstado(new Enviado());
        manager.getPaquetes().get(1003).setEstado(new EnReparto());

    }

    public void mainMenu() {
        datosDefault();
        String opt;
        do {
            System.out.printf("\n%s \n", "#".repeat(39));
            System.out.printf("# %37c\n", '#');
            System.out.printf("%-13c %-23s %1c\n", '#', "pqXpress", '#');
            System.out.printf("# %37c\n", '#');
            System.out.printf("# --->--->--->--->--->--->--->--->--- #\n");
            System.out.printf("# %37c\n", '#');
            System.out.printf("# %-35s %1c\n", "[1] - Alta cliente", '#');
            System.out.printf("# %-35s %1c\n", "[2] - Lista clientes", '#');
            System.out.printf("# %-35s %1c\n", "[3] - Alta nuevo envío", '#');
            System.out.printf("# %-35s %1c\n", "[4] - Consultar estado envio", '#');
            System.out.printf("# %-35s %1c\n", "[5] - Cambiar estado envio", '#');
            System.out.printf("# %-35s %1c\n", "[6] - Listar envíos por cliente", '#');
            System.out.printf("# %-35s %1c\n", "[7] - Listar envios en reparto", '#');
            System.out.printf("# %-35s %1c\n", "[X] - Salir", '#');
            System.out.printf("# %37c\n", '#');
            System.out.printf("%s \n", "#".repeat(39));
            System.out.print("Opcion > ");
            opt = s.nextLine().toUpperCase();
            opcionesMenu(opt);
        } while (!opt.equalsIgnoreCase("X"));
    }

    public void opcionesMenu(String opt) {

        if (opt.equals("X"))
            opt = "Salir";

        switch (opt) {
            case "1":
                System.out.println("Nuevo Cliente\n");
                System.out.print("Introudce NIF/DNI > ");
                String id = s.nextLine().toUpperCase().replaceAll("\\s", "");
                System.out.print("Introduce nombre > ");
                String name = s.nextLine();
                if (!idClienteRegistrado(id)) {
                    System.out.println("Dar de alta Cliente? [A]ceptar [C]ancelar ");
                    String conf = s.nextLine();
                    if (confirmar(conf)) {
                        altaCliente(id, name);
                        System.out.println("Cliente registrado correctamente");
                    } else
                        break;
                } else
                    System.out.println("el dni " + id + " coincide con otro registrado en el sistema");
                break;
            case "2":
                System.out.println(listaClientes());
                break;
            case "3":
                System.out.println("Nuevo envio\n");
                System.out.print("Introduce NIF/DNI > ");
                id = s.nextLine().toUpperCase().replaceAll("\\s", "");
                System.out.print("Introduce nombre cliente > ");
                name = s.nextLine();
                if (!idClienteRegistrado(id)) {
                    System.out.println("El cliente no esta en el sistema, se dara de alta\n" + "[A]ceptar [C]ancelar");
                    String conf = s.nextLine();
                    if (confirmar(conf)) {
                        manager.altaCliente(id, name);
                        System.out.println("Cliente registrado correctamente");
                    } else
                        break;
                } else {
                    System.out.print("Introduce nombre destinatario > ");
                    String para = s.nextLine().toUpperCase();
                    System.out.print("Introduce direccion de envio > ");
                    String dir = s.nextLine();
                    System.out.println("Dar de alta Envio? [A]ceptar [C]ancelar ");
                    String conf = s.nextLine();
                    if (confirmar(conf)) {
                        altaEnvio(id, name, para, dir);
                        System.out.println("Paquete registrado correctamente");
                    } else
                        break;
                }
                break;
            case "4":
                System.out.println("Estado del paqute\n");
                System.out.println(mostrarPaquetes());
                System.out.print("Introduce el identificador del paquete > ");
                try {
                    id = s.nextLine().replaceAll("\\s", "");
                    if (idPaqueteRegistrado(Integer.parseInt(id)))
                        System.out.println(consultarEstadoEnvio(id));
                    else
                        System.out.println("El identificador introducido no pertenece a ningun envio");
                } catch (NumberFormatException e) {
                    System.out.println(
                            "Datos incorrectos " + e.getMessage() + " Debes introducir el identificador de paquete");
                }
                break;
            case "5":
                System.out.println("Cambiar estado del paquete\n");
                System.out.println("Quiers [a]vanzar de estado o [e]legir estado manual?");
                String opc = s.nextLine();
                cambiarEstadoEnvio(opc);
                break;
            case "6":
                System.out.println("Ver los envios de un cliente\n");
                System.out.println(listaClientes());
                System.out.print("Introduve el NIF/DNI de cliente a consultar > ");
                id = s.nextLine();
                if (idClienteRegistrado(id))
                    System.out.println(listarEnviosPorCliente(id));
                else
                    System.out.println("No se ha encontrado a ningun cliente con ese DNI/NIF");
                break;
            case "7":
                System.out.println("Ver los envios en reparto\n");
                System.out.println(listarEnviosEnReparto());
                break;
            case "Salir":
                System.out.println("salir");
                break;
            default:
                System.out.println("Opcion no válida");
                break;
        }
    }

    private boolean confirmar(String opt) {
        return opt.toLowerCase().equals("a");
    }

    private boolean idClienteRegistrado(String pDni) {
        for (Cliente cli : manager.getClientes())
            if (pDni.equals(cli.getId()))
                return true;
        return false;
    }

    private boolean idPaqueteRegistrado(int pId) {
        return manager.getPaquetes().containsKey(pId);
    }

    private void altaCliente(String id, String name) {
        manager.altaCliente(id, name);
    }

    private String listaClientes() {
        String str = "";
        int cont = 0;
        for (Cliente cli : manager.listaClientes())
            str += (++cont) + " " + cli.toString() + "\n";

        return str;
    }

    private void altaEnvio(String id, String name, String para, String dir) {
        manager.altaEnvio(new Cliente(id, name), para, dir);
    }

    private String consultarEstadoEnvio(String pid) {
        int id = Integer.parseInt(pid);
        return manager.getPaquetes().get(id).informaEstado();
    }

    private String mostrarPaquetes() {
        String str = "";
        int cont = 0;

        Set<Integer> clave = manager.getPaquetes().keySet();
        for (Integer key : clave)
            if (key.equals(manager.getPaquetes().get(key).getId()))
                str += (++cont) + " " + manager.getPaquetes().get(key) + "\n";
        return str;
    }

    private void cambiarEstadoEnvio(String opt) {
        System.out.println(mostrarPaquetes());
        switch (opt.toLowerCase()) {
            case "a":
                System.out.print("Introduce identificador de paquete > ");
                try {
                    int idt = Integer.parseInt(s.nextLine());
                    if (idPaqueteRegistrado(idt))
                        manager.getPaquetes().get(idt).cambiaEstado();
                    else
                        System.out.println("El identificador introducido no pertenece a ningun envio");
                    System.out.println(manager.estadoPedido(idt));
                } catch (NumberFormatException e) {
                    System.out.println(
                            "Datos incorrectos " + e.getMessage() + " Debes introducir el identificador de paquete");
                }
                break;
            case "e":
                System.out.print("Introduce identificador de paquete > ");
                try {
                    int idt = Integer.parseInt(s.nextLine());
                    if (idPaqueteRegistrado(idt)) {
                        System.out.println("Selecciona estado:");
                        opcionesEstado();
                        int estado = Integer.parseInt(s.nextLine());
                        manager.getPaquetes().get(idt).setEstado(elijeEstado(estado));
                        System.out.println(manager.estadoPedido(idt));
                    } else
                        System.out.println("El identificador introducido no pertenece a ningun envio");
                } catch (NumberFormatException e) {
                    System.out.println(
                            "Datos incorrectos " + e.getMessage() + " Debes introducir el identificador de paquete");
                }
                break;
            default:
                System.out.println("Opcion no valida");
                break;
        }
    }

    private void opcionesEstado() {
        System.out.println("1.- Ordenado");
        System.out.println("2.- En proceso");
        System.out.println("3.- Enviado");
        System.out.println("4.- En reparto");
        System.out.println("5.- Entregado");
    }

    private EstadoPq elijeEstado(int opc) {
        EstadoPq estado = null;
        switch (opc) {
            case 1:
                estado = new Ordenado();
                break;
            case 2:
                estado = new EnProceso();
                break;
            case 3:
                estado = new Enviado();
                break;
            case 4:
                estado = new EnReparto();
                break;
            case 5:
                estado = new Entregado();
                break;
            default:
                System.out.println("Opcion no valida");
                break;
        }
        return estado;
    }

    private Cliente clienteConcreto(String id) {
        Cliente concreto = null;
        for (Cliente cli : manager.getClientes())
            if (id.equals(cli.getId()))
                concreto = cli;
        return concreto;
    }

    private String listarEnviosPorCliente(String id) {
        String str = "";
        int cont = 0;
        Paquete[] lista = manager.listaEnviosCliente(id);
        if (lista[0] == null)
            str += clienteConcreto(id).toString() + " no ha realizado ningun envio\n";
        else {
            for (Paquete paq : lista) {
                str += paq != null
                        ? (++cont) + "-> " + "Fecha de alta[" + paq.getFAlta() + "]\t DESTINO: " + paq.getDestino()
                                + "\t ESTADO: " + paq.informaEstado() + "\n"
                        : "";
            }
        }

        return str;
    }

    private String listarEnviosEnReparto() {
        String str = "";
        int cont = 0;
        Paquete[] reparto = manager.listaReparto();
        if (reparto[0] == null)
            str += "No hay envios en reparto\n";
        else {
            for (Paquete paq : reparto) {
                str += paq != null
                        ? (++cont) + "-> " + "Cliente[" + paq.getCliente() + "] Destino: " + paq.getDestino() + "\n"
                        : "";
            }
        }
        return str;
    }
}
